import express from "express";
import json from 'body-parser';
export const router = express.Router();

router.get('/', (req, res) => {
    
    const params = {
        num_docente: req.query.num_docente || '',
        nombre: req.query.nombre || '',
        domicilio: req.query.domicilio || '',
        nivel: req.query.nivel || '',
        pago_hora_base: req.query.pago_hora_base || '',
        horas_impartidas: req.query.horas_impartidas || '',
        num_hijos: req.query.num_hijos || '',
    }
    res.render('index',params);
});

router.post('/salida', (req, res) => {
    let incremento_nivel = req.body.nivel;
    let bono_hijos = req.body.num_hijos;
    incremento_nivel = nivel(incremento_nivel);
    bono_hijos = hijos(bono_hijos);

    const params = {
        pago_base: req.body.pago_hora_base,
        horas_impartidas: req.body.horas_impartidas,
        nivel: incremento_nivel,
        bono: bono_hijos
    }
    res.render('salida', params);
});


function nivel(incremento_nivel){

    if(incremento_nivel === '1'){
        incremento_nivel = 1.3;
    }
    else if(incremento_nivel === '2'){
        incremento_nivel = 1.5;
    }
    else if(incremento_nivel === '3'){
        incremento_nivel = 2;
    }
    return incremento_nivel
}

function hijos(hijos){
    if(hijos === '1'){
        hijos = 0;
    }
    else if(hijos === '2'){
        hijos = 0.05;
    }
    else if(hijos === '3'){
        hijos = 0.10;
    }
    else if(hijos === '4'){
        hijos = 0.2;
    }
    return hijos;
}

export default { router };